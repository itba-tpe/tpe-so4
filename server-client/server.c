#include "servcli.h"

/* STATIC VARIABLES */
static char * answers[TOPLEVEL] = {
    "entendido\n",
    "#0854780*\n",
    "nokia\n",
    "cabeza de calabaza\n",
    "easter_egg\n",
    ".runme\n",
    "indeterminado\n",
    "this is awesome\n",
    "cachiporra\n",
    "gdb rules\n",
    "/lib/x86_64-linux-gnu/ld-2.19.so\n"
};

typedef void (*question) (void);
static question questions[TOPLEVEL];

char keypad[ROWS][COLS] = {{'1','2','3'},
						   {'4','5','6'},
						   {'7','8','9'},
						   {'*','0','#'}};

static char * phonetic[26] = {
    "Alfa",
    "Bravo",
    "Charlie",
    "Delta",
    "Echo",
    "Foxtrot",
    "Golf",
    "Hotel",
    "India",
    "Juliett",
    "Kilo",
    "Lima",
    "Mike",
    "November",
    "Oscar",
    "Papa",
    "Quebec",
    "Romeo",
    "Sierra",
    "Tango",
    "Uniform",
    "Victor",
    "Whiskey",
    "X-ray",
    "Yankee",
    "Zulu"
};

int main() {
	int sockfd, newfd;
	struct  sockaddr_in server, client;
	socklen_t cliLen;

	initChallenges();

	/* Creation of socket
	 * PF_INET --> internet protocol family
	 * SOCK_STREAM --> it is a TCP socket
	 * protocol = IPPROTO_TCP --> to have socket() choose the correct protocol based on the type.
	 */

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd < 0) {
		perror("Socket creation failed");
		exit(EXIT_FAILURE);
	}

	// Zeroes out the whole struct, thus resetting the values
	bzero((char *) &server, sizeof(server));

	server.sin_family	= AF_INET;					// host byte order
	server.sin_addr.s_addr = htonl(INADDR_ANY);		// use my IP address (htonl converts from native byte order to network byte order)
	server.sin_port = htons(PORT);					// assigning the specific port

	/* So that it can reuse the address --> Avoids the "address already in use" error */
	int set, option = TRUE;
	set = setsockopt(sockfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), &option, sizeof(option));
	if (set < 0){
		perror("Setsockopt failed");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	/* Binding the socket to the servers address */
	if (bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0) {
		perror("Binding of socket failed");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	/* backlog = number of connections allowed on the incoming queue
	 * incoming connections will wait in queue until accepted
	 */

	if (listen(sockfd, BACKLOG) < 0){
		perror("Listen failed!\n");
		exit(EXIT_FAILURE);
	}

	/*
	 * Returns a brand new socket file descriptor ready to send() and recv()
	 * In client --> the info about the incoming connection
	 * cliLen tells me the max of bytes to put in client (if it puts less it changes the value)
	 */

	cliLen = sizeof(client);
	newfd = accept(sockfd, (struct sockaddr *)&client, &cliLen);
	if (newfd < 0){
		perror("Accept failed!\n");
		exit(EXIT_FAILURE);
	}

	serverChallenges(newfd);

	// Close connection on you socker descriptor
	shutdown(newfd, SHUT_RDWR);
	shutdown(sockfd, SHUT_RDWR);
	exit(EXIT_SUCCESS);
}

void serverChallenges(int newfd){
	char buffer[MAXBUFFER];
	int n, level = 0;

	while(level < TOPLEVEL) {
		printQuestion(level);

		n = readFromBuffer(newfd, buffer);		// Read answer given by client

		//Continue reading from the buffer until the right answer is found
		while (n != strlen(answers[level]) || strncmp(answers[level], buffer, n) != 0){
			auxPrint("Respuesta incorrecta: ", buffer);
			sleep(1);		//Leave time to read the previous print
			printQuestion(level);
			writeToBuffer(newfd, CONTINUE);
			n = readFromBuffer(newfd, buffer);
		}
		//Correct answer was written, pass to the next level
		auxPrint("Respuesta correcta: ", buffer);

		if(level + 1 == TOPLEVEL){
			// PRINT GOODBYE MESSAGE
			endChallenges();
			writeToBuffer(newfd, FINISH);	// Tell client the game is finished
		} else {
			writeToBuffer(newfd, CONTINUE);
		}

		sleep(1);
		level++;
	}
}

void printQuestion(int level) {
	write(STDOUT, CLEAR, CLEARSIZE);
	printf("------------- DESAFIO -------------\n");
	questions[level]();
}

void initChallenges() {
	questions[0] = beginningQ;
	questions[1] = telephoneQ;
	questions[2] = morseQ;
	questions[3] = ebadfQ;
	questions[4] = stringQ;
	questions[5] = dataQ;	//DONT KNOW WHAT NAME TO GIVE IT
	questions[6] = mixedfdQ;
	questions[7] = messageQ;
	questions[8] = quineQ;
	questions[9] = gdbmeQ;
    questions[10] = libraryQ;
}

void endChallenges() {
	char * byeMessage = "Felicitaciones, finalizaron el juego. Ahora deberán implementar el servidor que se comporte como el servidor provisto\n";
	write(STDOUT, CLEAR, CLEARSIZE);
	write(STDOUT, byeMessage, strlen(byeMessage));
}

void beginningQ() {
	printf("Bienvenidos al TP4 y felicitaciones, ya resolvieron el primer acertijo.\n\nEn este TP deberan finalizar el juego que ya comenzaron resolviendo los desafios de cada nivel.\nAdemás tendrán que investigar otras preguntas para responder durante la defensa.\nEl desafio final consiste en crear un servidor que se comporte igual que yo, ademas del cliente para comunicarse con el mismo.\n\nDeberan estar atentos a los desafios ocultos.\n\nPara verificar que sus respuestas tienen el formato correcto respondan a este desafio con \"entendido\\n\"\n\n");
}

/* ----------------------------------- START OF TELEPHONE --------------------------------------- */

void telephoneQ() { //answer = #0854780*
	int prevRow, prevCol, len;

	char curr, * answer = answers[TELEPHONE];
	char aux[2];
	len = strlen(answer);

	// Processing the first key so we they know where to start
	curr = answer[0];
	aux[0] = curr;
	aux[1] = '\0';
	auxPrint(aux, " ");
	getPosition(curr, &prevRow, &prevCol);

	for (int i = 1; i < len; i++) {
		curr = answer[i];

		// Based on where I used to be, see where I have to go to get to the new one
		// Will also update the prevCol and prevRow to the current one
		printDirection(&prevRow, &prevCol, curr);

		// Want to print last key before \n
		if(i == len - 2){
			aux[0] = curr;
			aux[1] = '\0';
			auxPrint(aux, " ");
		}
	}
	write(STDOUT, "\n\n", 2);
	return;
}

void printDirection(int * pRow, int * pCol, char currentKey){
	if (*pRow != 0){
		if (keypad[*pRow-1][*pCol] == currentKey){
			auxPrint(UP, " ");
			*pRow = *pRow - 1;
			return;
		}
	}
	if (*pRow != ROWS - 1){
		if (keypad[*pRow+1][*pCol] == currentKey){
			auxPrint(DOWN, " ");
			*pRow = *pRow + 1;
			return;
		}
	}
	if (*pCol != 0){
		if (keypad[*pRow][*pCol-1] == currentKey){
			auxPrint(LEFT, " ");
			*pCol -= 1;
			return;
		}
	}
	if (*pCol != COLS - 1){
		if (keypad[*pRow][*pCol+1] == currentKey){
			auxPrint(RIGHT, " ");
			*pCol += 1;
			return;
		}
	}
	return;
}

void getPosition(char key, int * row, int * col) {
	for (int i = 0; i < ROWS; i++){
		for (int j = 0; j < COLS; j++){
			if (key == keypad[i][j]){
				*row = i;
				*col = j;
			}
		}
	}
}

/* ----------------------------------- END OF TELEPHONE --------------------------------------- */

void morseQ() {
	printf("https://vocaroo.com/i/s19015zmR4t8\n\n");
}

void ebadfQ() {
	ebadf();
	printf("EBADF... abrilo y veras\n\n");
}
void ebadf(){
	char * str = answers[EBADF];
	write(5, str, strlen(str));		//Write to fd = 5
	return;
}

void stringQ() {
	char *cmd = "strings myServer";
	char buf[MAXBUFFER], * answer = answers[EASTEREGG];
	int counter = 0;
	FILE *ptr;

	// Runs command strings and finds the string number STRINGNUM
	if ((ptr = popen(cmd, READ)) != NULL) {
		while (fgets(buf, MAXBUFFER, ptr) != NULL){
			if (strncmp(buf, answer, strlen(answer)-1) == 0){ //-1 to remove the \n
				printf("respuesta = strings[%d]\n\n", counter);
				return;
			}
			counter++;
		}
		sleep(1);
		pclose(ptr);
	}
	return;
}

void dataQ() {
  printf(".init ");
  printf(".plt ");
  printf(".text ");
  printf(".fini ");
  printf(".rodata ");
  printf("? ");
  printf(".eh_frame_hdr ");
  printf(".eh_frame ");
  printf(".init_array ");
  printf(".fini_array ");

  printf("\n\n");
}

void mixedfdQ() {
	srand(time(NULL));		//Setting rand seed
	char * solution = "la respuesta a este acertijo es indeterminado";
	int times;
	//Start
	printf("mixed fds \n");

	char * c = solution;		//We are going to cycle on this string, so we set start and the first char
	while (*c != '\0') {
		//On each space we add X random letters, where X is a random number between 0 and 5
		times = rand() % 6;
		for (int i = 0; i < times; ++i) {
			char letter = 'a' + (char) (rand() % 26);	//Pick a random letter
			fputc(letter, stderr);					//Write that letter in stderrr
			//fputc('-', stdout);
		}
		fputc(*c, stdout);
		fflush(stdout);
		c++;
	}

	printf("\n\n");
}

void messageQ() {
    char * string = answers[7];
    int j = 0;


    for(int i = 0 ; i < strlen(string)-1 ; ++i){

        j = string[i] - 'a';

        if(j >= 0 && j<= MAXPHONETIC)
            printf("%s ",phonetic[j]);

    }


	printf("\n\n");
    return;
}

void quineQ() {
	char *gcc = "gcc quine.c -o quine";
	char *diff = "./quine | diff - quine.c";

	//Start
	printf("quine\n");

	//Call to gcc
	if (system(gcc) == 0) {
		printf("¡Genial!, ya lograron meter un programa en quine.c, veamos si hace lo que corresponde:\n");

		int retValue = system(diff);
		if (retValue == 0) {
			printf("Genial! la respuesta a este acertijo es cachiporra\n\n");
		} else {
			printf("diff returned: %d\n\n", retValue);
		}
	}
}

void gdbmeQ() {
    gdbme(0);
	printf("b gdbme y encontra el valor mágico\n\n");
}

void libraryQ(void) {

  int c,i=0;

  const char *find = "find /lib -name \"";

  FILE* output;

  char shellInput[MAXBUFFER];

  char buffer[MAXBUFFER];

  char * file;


    file = "libc-2.19.so";

    snprintf(shellInput, strlen(find) + strlen(file) + 2, "%s%s\"", find , file);

    output = popen(shellInput,"r");

    if(output == NULL){
        printf("/lib/x86_64-linux-gnu/libc-2.19.so ?\n\n");
    }

    while ((c = fgetc(output)) != EOF && i < MAXBUFFER - 1 ) {
        buffer[i++] = c;
    }
    buffer[i-1] = '\0';

    printf("%s",buffer);
    printf(" ? \n\n");


    pclose(output);
    return;
}

void gdbme(int n){
    if(n==1){
        printf("la respuesta a este acertijo es gdb rules\n");
    }
    return;
}

//Function that makes easter egg appear as a string in strings server
void easter_egg(){
	return;
}
