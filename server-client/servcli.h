
#ifndef SERVCLI_H
#define SERVCLI_H

#define _DEFAULT_SOURCE

//INCLUDES
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <strings.h> 
#include <unistd.h>
#include <sys/socket.h> 
#include <sys/types.h> 
#include <time.h>
#include "auxFunctions.h"


//DEFINES
#define PORT 6694
#define BACKLOG 1

#define TRUE 1
#define FALSE !TRUE

#define TOPLEVEL 11

#define CLEAR "\x1B[1;1H\x1B[2J"
#define CLEARSIZE 12
#define CONTINUE "1"
#define FINISH "0"

#define ROWS 4
#define COLS 3
#define UP "\\033[A"
#define DOWN "\\033[B"
#define RIGHT "\\033[C"
#define LEFT "\\033[D"
#define STRINGNUM 4
#define READ "r"
#define MAXPHONETIC 25

#define TELEPHONE 1
#define EBADF 3
#define EASTEREGG 4

void serverChallenges(int socketfd);
void printQuestion(int level);
void initChallenges(void);
void endChallenges();
void beginningQ(void);
void telephoneQ(void);
void morseQ(void);
void ebadfQ(void);
void stringQ(void);
void dataQ(void);
void mixedfdQ(void);
void messageQ(void);
void quineQ(void);
void gdbmeQ(void);
void libraryQ(void);

void clientChallenges(int sockfd);

void easter_egg();
void getPosition(char key, int * row, int * col);
void printDirection(int * pRow, int * pCol, char currentKey);
void ebadf(void);
void gdbme(int n);



/* struct sockaddr {
    unsigned short    sa_family;    // address family, AF_xxx
    char              sa_data[14];  // 14 bytes of protocol address
};

struct sockaddr_in {
    short            sin_family;   // e.g. AF_INET, AF_INET6
    unsigned short   sin_port;     // e.g. htons(3490)
    struct in_addr   sin_addr;     // see struct in_addr, below
    char             sin_zero[8];  // zero this if you want to
};
*/

#endif
