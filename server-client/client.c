#include "servcli.h"

int main(int argc, char *argv[]) {
	int sockfd;
	struct  sockaddr_in server;
	struct hostent * serverHost; /* Host info */

	/* Making sure the IP address was sent through the argument */
	if (argc < 2){
		perror("Hostname missing\n");
		exit(EXIT_FAILURE);
	}

	serverHost = gethostbyname(argv[1]);
	if(serverHost == NULL){
		perror("Error: No such host\n");
		exit(EXIT_FAILURE);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd < 0) {
		perror("Socket opening failed!\n");
		exit(EXIT_FAILURE);
	}

	/* Zeroes out the whole struct, thus resetting the values */
	bzero((char *) &server, sizeof(server));

	server.sin_family	= AF_INET;					// host byte order
	server.sin_port = htons(PORT);					// assigning the specific port

	/* Copies n bytes from serverHost to server */
	bcopy(serverHost -> h_addr_list[0], 
		 (char *) &server.sin_addr,
          serverHost -> h_length);

	/* Request connection with the host */
	if (connect(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0){
		perror("Connection failed!\n");
		exit(EXIT_FAILURE);
	} else {
		printf("Connect successful\n");
	}

	clientChallenges(sockfd);

	shutdown(sockfd, SHUT_RDWR);
	exit(EXIT_SUCCESS);
}

void clientChallenges(int sockfd){
	char buffer[MAXBUFFER];

	while(TRUE) {
		readFromBuffer(STDIN, buffer);
		writeToBuffer(sockfd, buffer);

		readFromBuffer(sockfd, buffer);

		if (strcmp(buffer, FINISH) == 0) break;
	}
}

