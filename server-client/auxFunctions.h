#ifndef AUXFUNCTIONS_H
#define AUXFUNCTIONS_H

#define MAXBUFFER 255
#define STDIN 0
#define STDOUT 1

int writeToBuffer(int fd, char * buffer);
int readFromBuffer(int fd, char * buffer);
void auxPrint(char * str, char * buf);

#endif