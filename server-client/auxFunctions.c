#include "auxFunctions.h"
#include <stdlib.h> 
#include <stdio.h>
#include <strings.h> 
#include <string.h> 
#include <unistd.h>

int readFromBuffer(int fd, char * buffer){
	int n;

	bzero(buffer, MAXBUFFER);				//Set the buffer where what is recieved will be stored to 0			
	n = read(fd, buffer, MAXBUFFER);		//Reads the buffer
	if (n < 0) {
		perror("Error on reading");	
		exit(EXIT_FAILURE);
	}
	return n;
}

// Reads from stdin what should be written in the buffer
int writeToBuffer(int fd, char * buffer){
	int n;

	n = write(fd, buffer, strlen(buffer));
	if (n < 0) perror("Error on writing");

	return n;
}

void auxPrint(char * str, char * buf){
	write(STDOUT, str, strlen(str));
	write(STDOUT, buf, strlen(buf));
}
