# TPE SO (4)

## Software

Install DOCKER (sudo pacman -S)

execute the following commands
docker pull agodio/itba-so:1.0
docker run -ti agodio/itba-so:1.0

make sure your docker image has gcc, make, nasm and git up to date (apt-get update && apt-get install inside the docker)

exit the container

now run
sudo ./docker.sh

## Makefile instuctions

clean: removes object, binary and out files

server: runs the server (not our server) on background

solve: compiles solver.c and runs it

all: compiles our server, our client and runs it
