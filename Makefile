#	Makefile for project

GCC=gcc
GCCFLAGS= -g -pedantic -Wall -std=c99
ASM=nasm
ASMFLAGS=-f elf64
ADDRESS= 0.0.0.0


.PHONY: solve
solve: server compile_solver run

compile_solver:
	$(GCC) $(GCCFLAGS) ./resources/solver.c -o mySolver

compile_server-client: compile_aux
	$(GCC) $(GCCFLAGS) ./server-client/auxFunctions.c aux_asm.o ./server-client/server.c -o myServer
	$(GCC) $(GCCFLAGS) ./server-client/auxFunctions.c ./server-client/client.c -o myClient

compile_aux:
	$(ASM) $(ASMFLAGS) ./server-client/aux.asm -o aux_asm.o

run:
	clear
	./mySolver

server:
	./resources/server &

.PHONY: all
all:	compile_server-client ourServer ourClient

.PHONY: compile
compile:	compile_solver compile_server-client

ourServer:
	./myServer &

ourClient:
	./myClient $(ADDRESS)

.PHONY: clean
clean:
	rm -rf resources/*.o *.o *.bin *.out mySolver myServer myClient
