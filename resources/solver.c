
#include "solver.h"

//server connection heavily influenced by socket tutorial available at geekforgeeks

int main(void){

    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[MAXBUFFER] = {0};
    
    char respuestas[][MAXBUFFER] = {
        R00,
        R01,
        R02,
        R03,
        R04,
        R05,
        R06,
        R07,
        R08,
        R09,
        R10
    };

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    //revamp later

    //looking horrible, but works
    
    printf("\nWelcome to solver, please tell me how many automatic solutions you want:\n");
    printf("\nMe: I want ");
    getInput(buffer);
    int progress =  atoi(buffer);
    
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    
    for(int i = 0 ; i < progress && i <= MAXCHALLENGE ; i++){
        send(sock, respuestas[i], strlen(respuestas[i]),0);
        sleep(1);
    }
    
    sleep(1);
    printf("\nFinished all automatic inputs, press ENTER to exit\n");


    while(getInput(buffer)){
        send(sock, buffer, strlen(buffer),0);
        sleep(1);
    }
    printf("\nReceived ENTER, preparing exit\n");

    if(shutdown(sock,SHUT_RDWR) != 0){
        printf("closing the socket failed\n");
    }

    return 0;
}


int getInput(char * string){
    int j = 0;
    int c = 0;

    while((c = getchar())!=-1 && c!='\n' && j < MAXBUFFER-1){
        string[j++] = c;
    }

    if(c=='\n'){
        string[j++] = c;
        if(j==1){return 0;}
    }

    string[j] = '\0';

    return 1;
}
