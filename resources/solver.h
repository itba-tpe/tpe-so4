#ifndef __SOLVER_H
#define __SOLVER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

//credit to geekforgeeks
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define MAXBUFFER 40
#define MAXCHALLENGE 10

#define R00 "entendido\n"
#define R01 "#0854780*\n"
#define R02 "nokia\n"
#define R03 "cabeza de calabaza\n"
#define R04 "easter_egg\n"
#define R05 ".runme\n"
#define R06 "indeterminado\n"
#define R07 "this is awesome\n"
#define R08 "cachiporra\n"
#define R09 "gdb rules\n"
#define R10 "/lib/x86_64-linux-gnu/ld-2.19.so\n"

#define PORT 23423


int getInput(char * string);


#endif
